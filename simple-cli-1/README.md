> 涉及技术

<img src="https://img.shields.io/badge/jdk-1.8-brightgreen">
<img src="https://img.shields.io/badge/mysql-8.0.13-brightgreen">
<img src="https://img.shields.io/badge/maven-3.6.1-brightgreen">
<br/>
<img src="https://img.shields.io/badge/springboot-2.2.7.RELEASE-brightgreen">
<img src="https://img.shields.io/badge/spring--data--jpa-2.2.7.RELEASE-brightgreen">
<br/>
<img src="https://img.shields.io/badge/swagger-3.0.0-brightgreen">
<img src="https://img.shields.io/badge/ok--util-1.0.0-brightgreen">
<img src="https://img.shields.io/badge/ok--jwt--interceptor-1.0.0-brightgreen">
<br/>

> 详细功能点

- 数据库自动创建
- 表自动创建
- 纯 Jpa Java 代码操作数据库及关联关系，无 HQL 语句；
- swagger api 文档；
- swagger api 鉴权；
- swagger api 排序（TODO）；
- restful api 严格按照 [ok-restful-api-spec](https://gitee.com/wudibo/ok-restful-api-spec) 标准；
