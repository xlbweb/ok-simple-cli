package cn.xlbweb.cli.common;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
@Data
@Component
public class CommonProperties {

    @Value("${server.port}")
    private String port;

    @Value("${cn.xlbweb.cli.swagger-uri}")
    private String swaggerUri;

    @Value("${cn.xlbweb.ji.token-name}")
    private String tokenName;
}
