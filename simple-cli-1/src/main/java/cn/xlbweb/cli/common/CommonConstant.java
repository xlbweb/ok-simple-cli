package cn.xlbweb.cli.common;

/**
 * @author: bobi
 * @date: 2018/7/16 14:52
 * @description:
 */
public class CommonConstant {

    /**
     * 超级管理员
     */
    public static final String ADMIN_USERNAME = "admin";

    /**
     * 默认排序字段
     */
    public static final String DEFAULT_ORDER_COLUMN = "createTime";
}
