package cn.xlbweb.cli.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: bobi
 * @date: 2020/12/2 上午11:56
 * @description:
 */
public class CommonEnum {

    @AllArgsConstructor
    @Getter
    public enum ResponseCode {
        /**
         * xxx
         */
        xxx(10, "xxx");

        private final Integer code;
        private final String message;
    }

    @AllArgsConstructor
    @Getter
    public enum UserStatus {
        /**
         * 账号正常
         */
        NORMAL(0, "账号正常"),
        /**
         * 账号未激活
         */
        NOT_ACTIVE(1, "账号未激活"),
        /**
         * 账号被禁用
         */
        DISABLE(2, "账号被禁用"),
        /**
         * 账号已删除
         */
        DELETE(3, "账号已删除");

        private int code;
        private String description;
    }
}
