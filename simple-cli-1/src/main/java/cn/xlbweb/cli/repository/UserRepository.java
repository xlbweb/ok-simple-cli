package cn.xlbweb.cli.repository;

import cn.xlbweb.cli.pojo.jdo.UserDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author: bobi
 * @date: 2020/11/30 下午12:16
 * @description:
 */
public interface UserRepository extends JpaRepository<UserDO, Integer>, JpaSpecificationExecutor<UserDO> {

    /**
     * 用户登录
     *
     * @param username
     * @param password
     * @return
     */
    UserDO findByUsernameAndPassword(String username, String password);

    /**
     * 根据账号查询用户
     *
     * @param username
     * @return
     */
    UserDO findByUsername(String username);
}
