package cn.xlbweb.cli.repository;

import cn.xlbweb.cli.pojo.jdo.RoleDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author: bobi
 * @date: 2020/11/30 下午12:16
 * @description:
 */
public interface RoleRepository extends JpaRepository<RoleDO, Integer>, JpaSpecificationExecutor<RoleDO> {

    /**
     * 根据角色名称查询
     *
     * @param roleName
     * @return
     */
    RoleDO findByRoleName(String roleName);
}
