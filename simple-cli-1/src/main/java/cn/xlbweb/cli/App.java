package cn.xlbweb.cli;

import cn.xlbweb.cli.common.CommonProperties;
import cn.xlbweb.util.AddrUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
@Slf4j
@EnableJpaAuditing
@SpringBootApplication
public class App implements ApplicationRunner {

    @Autowired
    private CommonProperties commonProperties;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("服务器地址: http://{}:{}", AddrUtils.getLocalIp(), commonProperties.getPort());
        log.info("API文档地址: http://{}:{}{}", AddrUtils.getLocalIp(), commonProperties.getPort(), commonProperties.getSwaggerUri());
    }

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
