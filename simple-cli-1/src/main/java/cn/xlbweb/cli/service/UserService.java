package cn.xlbweb.cli.service;

import cn.xlbweb.cli.pojo.dto.UserInsertDTO;
import cn.xlbweb.cli.pojo.dto.UserListDTO;
import cn.xlbweb.cli.pojo.dto.UserUpdateDTO;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
public interface UserService {

    /**
     * 查询当前登陆用户
     *
     * @return
     */
    ServerResponse getCurrentUser();

    /**
     * 根据id查询用户
     *
     * @param id
     * @return
     */
    ServerResponse getUser(Integer id);

    /**
     * 模糊查询用户
     *
     * @param dto
     * @return
     */
    TableResponse listUser(UserListDTO dto);

    /**
     * 添加用户
     *
     * @param dto
     * @return
     */
    ServerResponse insertUser(UserInsertDTO dto);

    /**
     * 更新用户
     *
     * @param dto
     * @return
     */
    ServerResponse updateUser(UserUpdateDTO dto);

    /**
     * 删除用户
     *
     * @param ids
     * @return
     */
    ServerResponse deleteUser(String ids);

    /**
     * 重置用户密码
     *
     * @param ids
     * @return
     */
    ServerResponse resetPwd(String ids);
}
