package cn.xlbweb.cli.service.impl;

import cn.xlbweb.cli.common.CommonConstant;
import cn.xlbweb.cli.repository.RoleRepository;
import cn.xlbweb.cli.pojo.dto.RoleInsertDTO;
import cn.xlbweb.cli.pojo.dto.RoleListDTO;
import cn.xlbweb.cli.pojo.dto.RoleUpdateDTO;
import cn.xlbweb.cli.pojo.jdo.RoleDO;
import cn.xlbweb.cli.pojo.vo.RoleVO;
import cn.xlbweb.cli.service.RoleService;
import cn.xlbweb.util.ConvertUtils;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public ServerResponse getRole(Integer id) {
        return ServerResponse.success("查询角色成功", roleRepository.getOne(id));
    }

    @Override
    public TableResponse listRole(RoleListDTO dto) {
        Specification<RoleDO> specification = (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (StringUtils.isNotBlank(dto.getName())) {
                predicates.add(criteriaBuilder.like(root.get("name"), "%" + dto.getName() + "%"));
            }
            if (StringUtils.isNotBlank(dto.getDescription())) {
                predicates.add(criteriaBuilder.like(root.get("description"), "%" + dto.getDescription() + "%"));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
        PageRequest pageRequest = PageRequest.of(dto.getPage() - 1, dto.getSize(), Sort.Direction.DESC, CommonConstant.DEFAULT_ORDER_COLUMN);
        Page<RoleDO> page = roleRepository.findAll(specification, pageRequest);
        List<RoleVO> roleVOs = ConvertUtils.copyListProperties(page.getContent(), RoleVO::new);
        return TableResponse.success("查询角色成功", page.getTotalElements(), roleVOs);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServerResponse insertRole(RoleInsertDTO dto) {
        RoleDO checkRoleDO = roleRepository.findByRoleName(dto.getName());
        if (Objects.nonNull(checkRoleDO)) {
            return ServerResponse.error("角色已存在");
        }
        RoleDO roleDO = new RoleDO();
        ConvertUtils.copyProperties(dto, roleDO);
        roleRepository.save(roleDO);
        return ServerResponse.success("添加角色成功");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServerResponse updateRole(RoleUpdateDTO dto) {
        RoleDO roleDO = roleRepository.getOne(dto.getId());
        ConvertUtils.copyProperties(dto, roleDO);
        roleRepository.save(roleDO);
        return ServerResponse.success("更新角色成功");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServerResponse deleteRole(String ids) {
        List<RoleDO> roleDOs = ConvertUtils.strToIntegerList(ids, RoleDO::new);
        roleRepository.deleteInBatch(roleDOs);
        return ServerResponse.success("删除角色成功");
    }
}
