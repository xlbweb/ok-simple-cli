package cn.xlbweb.cli.service;

import cn.xlbweb.cli.pojo.dto.RoleInsertDTO;
import cn.xlbweb.cli.pojo.dto.RoleListDTO;
import cn.xlbweb.cli.pojo.dto.RoleUpdateDTO;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
public interface RoleService {

    /**
     * 根据id查询角色
     *
     * @param id
     * @return
     */
    ServerResponse getRole(Integer id);

    /**
     * 模糊查询角色
     *
     * @param dto
     * @return
     */
    TableResponse listRole(RoleListDTO dto);

    /**
     * 添加角色
     *
     * @param dto
     * @return
     */
    ServerResponse insertRole(RoleInsertDTO dto);

    /**
     * 更新角色
     *
     * @param dto
     * @return
     */
    ServerResponse updateRole(RoleUpdateDTO dto);

    /**
     * 删除角色
     *
     * @param ids
     * @return
     */
    ServerResponse deleteRole(String ids);
}
