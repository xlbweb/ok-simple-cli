package cn.xlbweb.cli.service;

import cn.xlbweb.cli.pojo.dto.LoginDTO;
import cn.xlbweb.util.response.ServerResponse;

/**
 * @author: bobi
 * @date: 2021/1/31 下午10:17
 * @description:
 */
public interface LoginService {

    /**
     * 用户登录
     *
     * @param dto
     * @return
     */
    ServerResponse login(LoginDTO dto);
}
