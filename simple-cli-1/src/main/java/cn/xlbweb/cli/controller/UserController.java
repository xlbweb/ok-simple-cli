package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.pojo.dto.UserInsertDTO;
import cn.xlbweb.cli.pojo.dto.UserListDTO;
import cn.xlbweb.cli.pojo.dto.UserUpdateDTO;
import cn.xlbweb.cli.service.UserService;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author: bobi
 * @date: 2020/9/6 下午6:36
 * @description: TODO Swagger接口显示顺序问题...
 */
@Api(tags = "用户管理")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "查询当前登陆用户", notes = "无序传递参数")
    @GetMapping("/users/current-user")
    public ServerResponse getCurrentUser() {
        return userService.getCurrentUser();
    }

    @ApiOperation(value = "根据id查询用户", notes = "即用户主键id")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "用户id", required = true)})
    @GetMapping("/users/{id}")
    public ServerResponse getUser(@PathVariable Integer id) {
        return userService.getUser(id);
    }

    @ApiOperation(value = "模糊查询用户", notes = "根据账号/邮箱/昵称/角色名称模糊搜索")
    @GetMapping("/users")
    public TableResponse listUser(@Valid UserListDTO dto) {
        return userService.listUser(dto);
    }

    @ApiOperation(value = "添加用户", notes = "账号/密码/手机/邮箱必填，参数为json格式")
    @PostMapping("/user")
    public ServerResponse insertUser(@Valid @RequestBody UserInsertDTO dto) {
        return userService.insertUser(dto);
    }

    @ApiOperation(value = "更新用户", notes = "用户主键id必传，参数为json格式")
    @PutMapping("/user")
    public ServerResponse updateUser(@Valid @RequestBody UserUpdateDTO dto) {
        return userService.updateUser(dto);
    }

    @ApiOperation(value = "删除用户", notes = "用户ids必传")
    @ApiImplicitParams({@ApiImplicitParam(name = "ids", value = "用户ids, 如1,2,3", required = true)})
    @DeleteMapping("/user/{ids}")
    public ServerResponse deleteUser(@PathVariable String ids) {
        return userService.deleteUser(ids);
    }

    @ApiOperation(value = "重置用户密码", notes = "用户ids必传")
    @ApiImplicitParams({@ApiImplicitParam(name = "ids", value = "用户ids, 如1,2,3", required = true)})
    @PutMapping("/user/reset-pwd/{ids}")
    public ServerResponse resetPwd(@PathVariable String ids) {
        return userService.resetPwd(ids);
    }
}
