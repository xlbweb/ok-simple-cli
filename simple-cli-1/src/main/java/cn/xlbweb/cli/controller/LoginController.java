package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.pojo.dto.LoginDTO;
import cn.xlbweb.cli.service.LoginService;
import cn.xlbweb.util.response.ServerResponse;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author: bobi
 * @date: 2020/9/6 下午6:36
 * @description:
 */
@Api(tags = "登陆管理")
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("login")
    public ServerResponse login(@Valid @RequestBody LoginDTO dto) {
        return loginService.login(dto);
    }

    @DeleteMapping("logout")
    public ServerResponse logout() {
        return ServerResponse.success("退出成功");
    }
}
