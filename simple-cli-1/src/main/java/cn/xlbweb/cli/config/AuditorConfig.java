package cn.xlbweb.cli.config;

import cn.xlbweb.ji.JwtUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 * @author: bobi
 * @date: 2021/2/16 下午2:18
 * @description:
 */
@Configuration
public class AuditorConfig implements AuditorAware {

    /**
     * 创建人/更新人
     *
     * @return
     */
    @Override
    public Optional getCurrentAuditor() {
        String username = JwtUtils.getUsername();
        if (StringUtils.isNotBlank(username)) {
            return Optional.of(username);
        }
        return Optional.empty();
    }
}
