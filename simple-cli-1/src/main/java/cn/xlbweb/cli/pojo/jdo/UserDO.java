package cn.xlbweb.cli.pojo.jdo;

import lombok.Data;

import javax.persistence.*;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Entity
@Table(name = "t_user")
@Data
public class UserDO extends BaseDO {

    @Column(columnDefinition = "varchar(20) not null comment '账号'")
    private String username;

    @Column(columnDefinition = "varchar(60) not null comment '密码'")
    private String password;

    @Column(columnDefinition = "varchar(11) not null comment '手机'")
    private String phone;

    @Column(columnDefinition = "varchar(30) not null comment '邮箱'")
    private String mail;

    @Column(columnDefinition = "int(11) not null comment '状态(0:正常 1:未激活 2:禁用 3:删除)'")
    private Integer status;

    @OneToOne
    @JoinColumn(name = "roleId")
    private RoleDO role;
}