package cn.xlbweb.cli.pojo.jdo;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * @author: bobi
 * @date: 2021/2/16 下午2:08
 * @description:
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
public class BaseDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @CreatedDate
    @Column(columnDefinition = "datetime default null comment '创建时间'")
    private Date createTime;

    @LastModifiedDate
    @Column(columnDefinition = "datetime default null comment '更新时间'")
    private Date updateTime;

    @CreatedBy
    @Column(columnDefinition = "varchar(20) not null comment '创建人'")
    private String createBy;

    @LastModifiedBy
    @Column(columnDefinition = "varchar(20) not null comment '更新人'")
    private String updateBy;
}
