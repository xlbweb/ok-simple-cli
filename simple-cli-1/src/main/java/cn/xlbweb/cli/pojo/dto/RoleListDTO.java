package cn.xlbweb.cli.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class RoleListDTO {

    @ApiModelProperty("角色名称")
    private String name;

    @ApiModelProperty("角色描述")
    private String description;

    @ApiModelProperty("当前页码，默认1")
    private Integer page = 1;

    @ApiModelProperty("每页显示数量，默认10")
    private Integer size = 10;
}
