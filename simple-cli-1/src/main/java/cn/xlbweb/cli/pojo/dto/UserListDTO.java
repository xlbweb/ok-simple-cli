package cn.xlbweb.cli.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class UserListDTO {

    @ApiModelProperty("账号")
    private String username;

    @ApiModelProperty("邮箱")
    private String mail;

    @ApiModelProperty("角色名称")
    private String roleName;

    @ApiModelProperty("昵称")
    private String nickname;

    @ApiModelProperty("当前页码，默认1")
    private Integer page = 1;

    @ApiModelProperty("每页显示数量，默认10")
    private Integer size = 10;
}
