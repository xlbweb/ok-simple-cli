package cn.xlbweb.cli.pojo.jdo;

import lombok.Data;

import javax.persistence.*;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Entity
@Table(name = "t_role")
@Data
public class RoleDO extends BaseDO {

    @Column(columnDefinition = "varchar(20) not null comment '角色名称'")
    private String roleName;

    @Column(columnDefinition = "varchar(30) not null comment '角色描述'")
    private String roleDesc;
}