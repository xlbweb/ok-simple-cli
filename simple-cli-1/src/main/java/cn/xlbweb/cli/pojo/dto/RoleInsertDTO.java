package cn.xlbweb.cli.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class RoleInsertDTO {

    @ApiModelProperty("角色名称")
    @NotNull(message = "角色名称不能为空")
    private String name;

    @ApiModelProperty("角色描述")
    @NotNull(message = "角色描述不能为空")
    private String description;
}
