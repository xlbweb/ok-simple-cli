-- 角色表
delete from t_role;
insert into t_role(id, role_name, role_desc) values(1, "ADMIN", "超级管理员");
insert into t_role(id, role_name, role_desc) values(2, "MANAGER", "普通管理员");
insert into t_role(id, role_name, role_desc) values(3, "MEMBER", "普通用户");

-- 用户表
delete from t_user;
insert into t_user(username, password, mail, phone, status, role_id) values("zhangsna", "123456", "zhangsan@qq.com", "13222222222", 0, 1);
insert into t_user(username, password, mail, phone, status, role_id) values("lisi", "123456", "lisi@qq.com", "13233333333", 0, 2);
insert into t_user(username, password, mail, phone, status, role_id) values("wangwu", "123456", "wangwu@qq.com", "13244444444", 0, 3);
insert into t_user(username, password, mail, phone, status, role_id) values("zhaoliu", "123456", "zhaoliu@qq.com", "13255555555", 0, 3);