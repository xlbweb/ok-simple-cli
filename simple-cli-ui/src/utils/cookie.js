import cookies from 'js-cookie'

const tokenName = process.env.VUE_APP_TOKEN_NAME

/**
 * 获取Token
 * @returns {*}
 */
function getToken() {
  return cookies.get(tokenName)
}

/**
 * 添加Token
 * @param token
 * @returns {*}
 */
function setToken(token) {
  return cookies.set(tokenName, token, { sameSite: 'strict' })
}

/**
 * 删除Token
 * @returns {*}
 */
function removeToken() {
  return cookies.remove(tokenName)
}

export {
  getToken,
  setToken,
  removeToken
}
