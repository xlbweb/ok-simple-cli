import { MessageBox } from 'element-ui'

/**
 * 针对Element UI组件的一些封装
 */
export default {
  // 确认消息对话框
  confirm(config = {}) {
    return new Promise((resolve, reject) => {
      // 自定义配置拼接
      const configTmp = {
        type: 'warning',
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        closeOnClickModal: false,
        config
      }
      let afterCloseResolve = () => {
      }
      // 拼接beforeClose
      configTmp.beforeClose = (action, instance, done) => {
        if (action === 'confirm') {
          instance.confirmButtonLoading = true
          config.confirmCallBack({
            loadingClose: () => new Promise((resolve, reject) => {
              done()
              instance.confirmButtonLoading = false
              afterCloseResolve = resolve
            }),
            action
          })
        } else {
          done()
        }
      }
      // 拼接confirm
      return MessageBox.confirm('确定执行此操作吗？', '提示', configTmp).then(() => {
        afterCloseResolve()
        resolve()
      }).catch(error => {
        afterCloseResolve()
        reject(error)
      })
    })
  }
}
