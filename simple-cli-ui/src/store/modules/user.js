const state = {
  /**
   * 账号
   */
  username: '',
  /**
   * 头像
   */
  avatar: ''
}

const mutations = {
  setUsername: (state, username) => {
    state.username = username
  },
  setAvatar: (state, avatar) => {
    state.avatar = avatar
  }
}

const actions = {
  currentUserInfo({ commit }, data) {
    commit('setUsername', data.username)
    commit('setAvatar', data.avatar)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

