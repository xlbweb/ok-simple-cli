const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  // 用户信息
  username: state => state.user.username,
  avatar: state => state.user.avatar
}
export default getters
