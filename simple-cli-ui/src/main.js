import Vue from 'vue'

import 'normalize.css/normalize.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/index.scss'

import App from './App'
import store from './store'
import router from './router'

import '@/icons'
import '@/router/auth'
import http from '@/utils/http'
import ComponentUtils from '@/utils/ComponentUtils'

Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.prototype.http = http
Vue.prototype.ComponentUtils = ComponentUtils

console.log(`node_env=${process.env.NODE_ENV}`)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
