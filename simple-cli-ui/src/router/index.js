import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Layout from '@/layout'

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/user/login'),
    hidden: true
  },

  {
    path: '/401',
    component: () => import('@/views/401'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: '/dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '控制台', icon: 'dashboard' }
    }]
  },

  {
    path: '/system',
    component: Layout,
    redirect: '/system/user',
    meta: { title: '系统管理', icon: 'example' },
    children: [
      {
        path: 'user',
        component: () => import('@/views/system/user'),
        meta: { title: '用户管理', icon: 'user' }
      },
      {
        path: 'role',
        component: () => import('@/views/system/role'),
        meta: { title: '角色管理', icon: 'peoples' }
      },
      {
        path: 'website',
        component: () => import('@/views/system/website'),
        meta: { title: '网站管理', icon: 'form' }
      }
    ]
  },

  {
    path: '/user',
    component: Layout,
    meta: { title: '用户管理'},
    hidden: true,
    children: [
      {
        path: 'info',
        component: () => import('@/views/user/info'),
        meta: { title: '个人中心'}
      }
    ]
  },

  {
    path: '/api',
    component: Layout,
    children: [
      {
        path: 'http://127.0.0.1:8080/swagger-ui/index.html',
        meta: { title: '接口文档', icon: 'link' }
      }
    ]
  },

  {
    path: '/gitee',
    component: Layout,
    children: [
      {
        path: 'https://gitee.com/wudibo/ok-simple-cli',
        meta: { title: '我的码云', icon: 'link' }
      }
    ]
  },

  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

const createRouter = () => new Router({
  // mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher
}

export default router
