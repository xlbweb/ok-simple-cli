module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  extends: ['plugin:vue/recommended', 'eslint:recommended'],
  rules: {
    // off
    'no-console': 'off',
    // error
    'indent': ['error', 2, {
      'SwitchCase': 1
    }],
    'vue/max-attributes-per-line': ['error', {
      'singleline': 10,
      'multiline': {
        'max': 1,
        'allowFirstLine': false
      }
    }],
  }
}
