package cn.xlbweb.cli.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: bobi
 * @date: 2020/12/2 上午11:56
 * @description:
 */
public class CliEnum {

    @AllArgsConstructor
    @Getter
    public enum ResponseCode {
        /**
         * xxx
         */
        xxx(10, "xxx");

        private final Integer code;
        private final String message;
    }

    @AllArgsConstructor
    @Getter
    public enum UserStatus {
        /**
         * 账号正常
         */
        NORMAL(0, "账号正常"),
        /**
         * 账号未激活
         */
        NOT_ACTIVE(1, "账号未激活"),
        /**
         * 账号被禁用
         */
        DISABLE(2, "账号被禁用"),
        /**
         * 账号已删除
         */
        DELETE(3, "账号已删除");

        private int code;
        private String description;
    }

//    @AllArgsConstructor
//    @Getter
//    public enum Role {
//        /**
//         * 超级管理员
//         */
//        ADMIN("ADMIN", "超级管理员"),
//        /**
//         * 普通管理员
//         */
//        MANAGER("MANAGER", "普通管理员"),
//        /**
//         * 普通成员
//         */
//        MEMBER("MEMBER", "普通成员");
//
//        private String name;
//        private String description;
//    }
}
