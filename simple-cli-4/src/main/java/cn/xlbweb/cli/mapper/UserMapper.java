package cn.xlbweb.cli.mapper;

import cn.xlbweb.cli.pojo.jdo.UserDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author: bobi
 * @date: 2020/11/30 下午12:16
 * @description:
 */
public interface UserMapper extends BaseMapper<UserDO> {

}
