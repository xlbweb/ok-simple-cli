package cn.xlbweb.cli;

import cn.xlbweb.util.IpUtils;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
@Slf4j
@MapperScan("cn.xlbweb.cli.mapper")
@SpringBootApplication
public class App implements ApplicationRunner {

    @Value("${server.port}")
    public int port;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Tomcat: http://{}:{}", IpUtils.getLocalIp(), port);
    }

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
