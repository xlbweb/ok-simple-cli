package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.pojo.dto.UserInsertDTO;
import cn.xlbweb.cli.pojo.dto.UserListDTO;
import cn.xlbweb.cli.pojo.dto.UserUpdateDTO;
import cn.xlbweb.cli.service.UserService;
import cn.xlbweb.util.res.ResponseServer;
import cn.xlbweb.util.res.ResponseTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author: bobi
 * @date: 2020/9/6 下午6:36
 * @description:
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users/current-user")
    public ResponseServer getCurrentUser() {
        return userService.getCurrentUser();
    }

    @GetMapping("/users/{id}")
    public ResponseServer getUser(@PathVariable Integer id) {
        return userService.getUser(id);
    }

    @GetMapping("/users")
    public ResponseTable listUser(@Valid UserListDTO dto) {
        return userService.listUser(dto);
    }

    @PostMapping("/user")
    public ResponseServer insertUser(@Valid @RequestBody UserInsertDTO dto) {
        return userService.insertUser(dto);
    }

    @PutMapping("/user")
    public ResponseServer updateUser(@Valid @RequestBody UserUpdateDTO dto) {
        return userService.updateUser(dto);
    }

    @DeleteMapping("/user/{ids}")
    public ResponseServer deleteUser(@PathVariable String ids) {
        return userService.deleteUser(ids);
    }

    @PutMapping("/user/reset-pwd/{ids}")
    public ResponseServer resetPwd(@PathVariable String ids) {
        return userService.resetPwd(ids);
    }
}
