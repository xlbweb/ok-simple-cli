package cn.xlbweb.cli.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class LoginDTO {

    @ApiModelProperty("账号")
    @NotNull(message = "账号不能为空")
    private String username;

    @ApiModelProperty("密码")
    @NotNull(message = "密码不能为空")
    private String password;
}
