package cn.xlbweb.cli.pojo.jdo;

import lombok.Data;

import java.util.Date;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class RoleDO {

    private Integer id;

    private String roleName;

    private String roleDesc;

    private Date createTime;

    private Date updateTime;
}