package cn.xlbweb.cli.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class UserListDTO {

    @ApiModelProperty("账号")
    private String username;

    @ApiModelProperty("邮箱")
    private String mail;

    @ApiModelProperty("昵称")
    private String nickname;

    @ApiModelProperty("当前页码")
    @NotNull(message = "当前页码不能为空")
    @Min(value = 1, message = "当前页码不能小于1")
    private Integer page = 1;

    @ApiModelProperty("每页显示数量")
    @NotNull(message = "每页显示数量不能为空")
    @Min(value = 1, message = "每页显示数量不能小于1")
    private Integer size = 10;
}
