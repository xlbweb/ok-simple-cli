package cn.xlbweb.cli.pojo.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
@Data
public class RoleVO {

    private Integer id;

    private String name;

    private String description;

    private Integer status;

    private Date createTime;

    private Date updateTime;
}
