package cn.xlbweb.cli.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class UserUpdateDTO {

    @ApiModelProperty("主键ID")
    @NotNull(message = "主键ID不能为空")
    private Integer id;

    @ApiModelProperty("电话")
    @NotNull(message = "电话不能为空")
    private String phone;

    @ApiModelProperty("邮箱")
    @NotNull(message = "邮箱不能为空")
    private String mail;
}
