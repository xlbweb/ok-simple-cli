package cn.xlbweb.cli.service.impl;

import cn.xlbweb.cli.mapper.RoleMapper;
import cn.xlbweb.cli.mapper.UserMapper;
import cn.xlbweb.cli.pojo.dto.LoginDTO;
import cn.xlbweb.cli.service.LoginService;
import cn.xlbweb.util.res.ResponseServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: bobi
 * @date: 2021/1/31 下午10:17
 * @description:
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public ResponseServer<String> login(LoginDTO dto) {
        return null;
    }
}
