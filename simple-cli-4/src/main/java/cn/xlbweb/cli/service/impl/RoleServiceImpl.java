package cn.xlbweb.cli.service.impl;

import cn.xlbweb.cli.mapper.RoleMapper;
import cn.xlbweb.cli.pojo.dto.RoleInsertDTO;
import cn.xlbweb.cli.pojo.dto.RoleListDTO;
import cn.xlbweb.cli.pojo.dto.RoleUpdateDTO;
import cn.xlbweb.cli.pojo.vo.RoleVO;
import cn.xlbweb.cli.service.RoleService;
import cn.xlbweb.util.res.ResponseServer;
import cn.xlbweb.util.res.ResponseTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public ResponseServer getRole(Integer id) {
        return null;
    }

    @Override
    public ResponseTable<List<RoleVO>> listRole(RoleListDTO dto) {
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseServer insertRole(RoleInsertDTO dto) {
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseServer updateRole(RoleUpdateDTO dto) {
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseServer deleteRole(String ids) {
        return null;
    }
}
