package cn.xlbweb.cli.service.impl;

import cn.xlbweb.cli.mapper.UserMapper;
import cn.xlbweb.cli.pojo.dto.UserInsertDTO;
import cn.xlbweb.cli.pojo.dto.UserListDTO;
import cn.xlbweb.cli.pojo.dto.UserUpdateDTO;
import cn.xlbweb.cli.pojo.jdo.UserDO;
import cn.xlbweb.cli.service.UserService;
import cn.xlbweb.util.res.ResponseServer;
import cn.xlbweb.util.res.ResponseTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public ResponseServer getCurrentUser() {
        return null;
    }

    @Override
    public ResponseServer getUser(Integer id) {
        return null;
    }

    @Override
    public ResponseTable listUser(UserListDTO dto) {
        List<UserDO> userDOs = userMapper.selectList(null);
        return ResponseTable.success(9999, userDOs);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseServer insertUser(UserInsertDTO dto) {
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseServer updateUser(UserUpdateDTO dto) {
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseServer deleteUser(String ids) {
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseServer resetPwd(String ids) {
        return null;
    }
}
