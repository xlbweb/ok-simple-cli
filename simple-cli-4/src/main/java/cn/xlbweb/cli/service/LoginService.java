package cn.xlbweb.cli.service;

import cn.xlbweb.cli.pojo.dto.LoginDTO;
import cn.xlbweb.util.res.ResponseServer;

/**
 * @author: bobi
 * @date: 2021/1/31 下午10:17
 * @description:
 */
public interface LoginService {

    /**
     * 用户登录
     *
     * @param dto
     * @return
     */
    ResponseServer<String> login(LoginDTO dto);
}
