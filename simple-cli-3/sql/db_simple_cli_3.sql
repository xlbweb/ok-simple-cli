/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : 127.0.0.1
 Source Database       : db_simple_cli_3

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : utf-8

 Date: 02/20/2021 15:59:06 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(20) DEFAULT NULL COMMENT '账号',
  `password` varchar(60) DEFAULT NULL COMMENT '密码',
  `phone` varchar(11) DEFAULT NULL,
  `mail` varchar(30) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
--  Records of `t_user`
-- ----------------------------
BEGIN;
INSERT INTO `t_user` VALUES ('1', 'admin', '111111', null, 'admin@qq.com', '2021-02-19 23:05:44', '2021-02-19 23:05:49'), ('2', 'zhangsan', '111111', null, 'zhangsan@163.com', '2021-02-19 23:05:59', '2021-02-19 23:06:03'), ('3', 'wangwu', '123456', null, 'wangwu@qq.com', null, null), ('6', 'wangwu', '123456', '13200000000', 'wangwuxx@qq.com', '2021-02-20 15:58:17', '2021-02-20 15:58:49');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
