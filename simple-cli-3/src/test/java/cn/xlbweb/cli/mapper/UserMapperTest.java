package cn.xlbweb.cli.mapper;

import cn.xlbweb.cli.AppTests;
import cn.xlbweb.cli.pojo.dto.UserInsertDTO;
import cn.xlbweb.cli.pojo.dto.UserUpdateDTO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author: bobi
 * @date: 2021/2/20 下午3:22
 * @description:
 */
public class UserMapperTest extends AppTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testInsertUser() {
        UserInsertDTO dto = new UserInsertDTO();
        dto.setUsername("wangwu");
        dto.setPassword("123456");
        dto.setMail("wangwu@qq.com");
        dto.setPhone("13255555555");
        int count = userMapper.insertUser(dto);
        System.out.println("影响条数=" + count);
        System.out.println("生成的主键ID=" + dto.getGenId());
    }

    @Test
    public void testUpdateUser() {
        UserUpdateDTO dto = new UserUpdateDTO();
        dto.setId(6);
        dto.setPhone("13200000000");
        dto.setMail("wangwuxx@qq.com");
        int count = userMapper.updateUser(dto);
        System.out.println("影响条数=" + count);
    }

    @Test
    public void testDeleteUser() {
        int count = userMapper.deleteUser("4,5,6");
        System.out.println("影响条数=" + count);
    }
}