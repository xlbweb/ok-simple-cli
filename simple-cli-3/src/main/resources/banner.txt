${AnsiColor.BRIGHT_RED}
        __                    .__               .__                          .__  .__
  ____ |  | __           _____|__| _____ ______ |  |   ____             ____ |  | |__|
 /  _ \|  |/ /  ______  /  ___/  |/     \\____ \|  | _/ __ \   ______ _/ ___\|  | |  |
(  <_> )    <  /_____/  \___ \|  |  Y Y  \  |_> >  |_\  ___/  /_____/ \  \___|  |_|  |
 \____/|__|_ \         /____  >__|__|_|  /   __/|____/\___  >          \___  >____/__|
            \/              \/         \/|__|             \/               \/
Author: wudibo
Email: super-wudibo@foxmail.com
SpringBoot Version: ${spring-boot.version}
${AnsiColor.DEFAULT}