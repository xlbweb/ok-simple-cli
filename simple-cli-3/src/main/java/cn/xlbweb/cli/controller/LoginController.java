package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.pojo.dto.LoginDTO;
import cn.xlbweb.cli.service.LoginService;
import cn.xlbweb.util.res.ResponseServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author: bobi
 * @date: 2020/9/6 下午6:36
 * @description:
 */
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("login")
    public ResponseServer login(@Valid @RequestBody LoginDTO dto) {
        return loginService.login(dto);
    }

    @DeleteMapping("logout")
    public ResponseServer logout() {
        return ResponseServer.success("退出成功");
    }
}
