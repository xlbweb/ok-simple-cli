package cn.xlbweb.cli.service;

import cn.xlbweb.cli.pojo.dto.UserInsertDTO;
import cn.xlbweb.cli.pojo.dto.UserListDTO;
import cn.xlbweb.cli.pojo.dto.UserUpdateDTO;
import cn.xlbweb.util.res.ResponseServer;
import cn.xlbweb.util.res.ResponseTable;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
public interface UserService {

    /**
     * 查询当前登陆用户
     *
     * @return
     */
    ResponseServer getCurrentUser();

    /**
     * 根据id查询用户
     *
     * @param id
     * @return
     */
    ResponseServer getUser(Integer id);

    /**
     * 模糊查询用户
     *
     * @param dto
     * @return
     */
    ResponseTable listUser(UserListDTO dto);

    /**
     * 添加用户
     *
     * @param dto
     * @return
     */
    ResponseServer insertUser(UserInsertDTO dto);

    /**
     * 更新用户
     *
     * @param dto
     * @return
     */
    ResponseServer updateUser(UserUpdateDTO dto);

    /**
     * 删除用户
     *
     * @param ids
     * @return
     */
    ResponseServer deleteUser(String ids);

    /**
     * 重置密码
     *
     * @param ids
     * @return
     */
    ResponseServer resetPwd(String ids);
}
