package cn.xlbweb.cli.service.impl;

import cn.xlbweb.cli.mapper.LoginMapper;
import cn.xlbweb.cli.pojo.dto.LoginDTO;
import cn.xlbweb.cli.service.LoginService;
import cn.xlbweb.util.res.ResponseServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * @author: bobi
 * @date: 2021/1/31 下午10:17
 * @description:
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginMapper loginMapper;

    @Override
    public ResponseServer login(LoginDTO dto) {
        Map<String, Object> result = loginMapper.login(dto);
        if (!CollectionUtils.isEmpty(result)) {
            return ResponseServer.success("登陆成功", result);
        }
        return ResponseServer.error("账号或密码错误");
    }
}
