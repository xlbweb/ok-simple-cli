package cn.xlbweb.cli.mapper;

import cn.xlbweb.cli.pojo.dto.LoginDTO;

import java.util.Map;

/**
 * @author: bobi
 * @date: 2021/2/21 下午12:30
 * @description:
 */
public interface LoginMapper {

    Map<String, Object> login(LoginDTO dto);
}
