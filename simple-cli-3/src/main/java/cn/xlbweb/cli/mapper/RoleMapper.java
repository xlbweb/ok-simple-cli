package cn.xlbweb.cli.mapper;

import cn.xlbweb.cli.pojo.dto.*;

import java.util.List;
import java.util.Map;

/**
 * @author: bobi
 * @date: 2020/11/30 下午12:16
 * @description:
 */
public interface RoleMapper {

    Map<String, Object> getRole(Integer id);

    List<Map<String, Object>> listRole(RoleListDTO dto);

    int insertRole(RoleInsertDTO dto);

    int updateRole(RoleUpdateDTO dto);

    int deleteRole(String ids);
}
