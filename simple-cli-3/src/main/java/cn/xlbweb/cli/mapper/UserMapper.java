package cn.xlbweb.cli.mapper;

import cn.xlbweb.cli.pojo.dto.UserInsertDTO;
import cn.xlbweb.cli.pojo.dto.UserListDTO;
import cn.xlbweb.cli.pojo.dto.UserUpdateDTO;

import java.util.List;
import java.util.Map;

/**
 * @author: bobi
 * @date: 2020/11/30 下午12:16
 * @description:
 */
public interface UserMapper {

    Map<String, Object> getUser(Integer id);

    List<Map<String, Object>> listUser(UserListDTO dto);

    int insertUser(UserInsertDTO dto);

    int updateUser(UserUpdateDTO dto);

    int deleteUser(String ids);
}
