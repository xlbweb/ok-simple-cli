package cn.xlbweb.cli.pojo.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class RoleListDTO {

    private String name;

    private String description;

    @NotNull(message = "当前页码不能为空")
    @Min(value = 1, message = "当前页码不能小于1")
    private Integer page = 1;

    @NotNull(message = "每页显示数量不能为空")
    @Min(value = 1, message = "每页显示数量不能小于1")
    private Integer size = 10;
}
