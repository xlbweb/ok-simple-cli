package cn.xlbweb.cli.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class LoginDTO {

    @NotNull(message = "账号不能为空")
    private String username;

    @NotNull(message = "密码不能为空")
    private String password;
}
