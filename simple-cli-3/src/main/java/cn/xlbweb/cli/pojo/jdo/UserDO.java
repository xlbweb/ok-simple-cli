package cn.xlbweb.cli.pojo.jdo;

import lombok.Data;

import java.util.Date;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class UserDO {

    private Integer id;

    private String username;

    private String password;

    private String phone;

    private String mail;

    private Integer status;

    private Integer roleId;

    private Date createTime;

    private Date updateTime;
}