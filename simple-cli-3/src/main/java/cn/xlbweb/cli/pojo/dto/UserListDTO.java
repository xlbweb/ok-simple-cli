package cn.xlbweb.cli.pojo.dto;

import lombok.Data;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class UserListDTO {

    private String username;

    private String mail;

    private String nickname;

    private Integer page = 1;

    private Integer size = 10;
}
