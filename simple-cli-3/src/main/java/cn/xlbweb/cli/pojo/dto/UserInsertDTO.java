package cn.xlbweb.cli.pojo.dto;

import cn.xlbweb.util.RegexUtils;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class UserInsertDTO {

    private Integer genId;

    @NotNull(message = "账号不能为空")
    @Length(min = 5, max = 20, message = "账号长度为5-20之间")
    private String username;

    @NotNull(message = "密码不能为空")
    private String password;

    @NotNull(message = "电话不能为空")
    private String phone;

    @NotNull(message = "邮箱不能为空")
    @Pattern(regexp = RegexUtils.MAIL, message = "邮箱格式不正确")
    private String mail;
}
