package cn.xlbweb.cli.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class RoleUpdateDTO {

    @NotNull(message = "主键ID不能为空")
    private Integer id;

    @NotNull(message = "角色名称不能为空")
    private String name;

    @NotNull(message = "角色描述不能为空")
    private String description;

    @NotNull(message = "角色状态不能为空")
    private String status;
}
