package cn.xlbweb.cli.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class UserUpdateDTO {

    @NotNull(message = "主键ID不能为空")
    private Integer id;

    @NotNull(message = "电话不能为空")
    private String phone;

    @NotNull(message = "邮箱不能为空")
    private String mail;
}
