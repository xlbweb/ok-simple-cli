<p align="center">
    <img src="https://images.gitee.com/uploads/images/2020/1204/225553_af898365_1152471.png"/>
    <p align="center">
        一个最基础的前后端分离开发脚手架！
    </p>
    <p align="center">
        <img src="https://img.shields.io/badge/jdk-1.8-brightgreen">
        <img src="https://img.shields.io/badge/mysql-8.0.13-brightgreen">
        <img src="https://img.shields.io/badge/maven-3.6.1-brightgreen">
        <img src="https://img.shields.io/badge/node-12.16.1-brightgreen">
        <img src="https://img.shields.io/badge/npm-6.13.4-brightgreen">
        <img src="https://img.shields.io/badge/vue--cli-4.2.3-brightgreen">
    </p>
    <p align="center">
        <img src="https://img.shields.io/badge/SpringBoot-2.2.5.RELEASE-brightgreen">
        <img src="https://img.shields.io/badge/JPA-2.2.5-brightgreen">
        <img src="https://img.shields.io/badge/Element%20UI-2.13.0-brightgreen">
        <img src="https://img.shields.io/badge/LICENSE-MIT-yellowgreen">
    </p>
</p>

---

> 关于数据库持久层操作的说明（重要！！！）

- 纯 JPA 代码版本：https://gitee.com/wudibo/ok-simple-cli/tree/master/simple-cli-1
- JPA 代码 + HQL 版本：https://gitee.com/wudibo/ok-simple-cli/tree/master/simple-cli-2
- Mybatis 版本：https://gitee.com/wudibo/ok-simple-cli/tree/master/simple-cli-3
- Mybatis-Plus版本：https://gitee.com/wudibo/ok-simple-cli/tree/master/simple-cli-4（后续新功能试点均在此版本进行）