/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : 127.0.0.1
 Source Database       : db_cli_simple

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : utf-8

 Date: 09/09/2020 23:37:17 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` varchar(19) COLLATE utf8mb4_bin NOT NULL COMMENT '主键ID',
  `username` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '账号',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '密码',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机',
  `email` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '邮箱',
  `nickname` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '昵称',
  `sex` int(11) DEFAULT '0' COMMENT '性别 0:男 1:女',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像',
  `status` int(11) DEFAULT '1' COMMENT '状态 0:正常 1:禁用 2:删除',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户表';

-- ----------------------------
--  Records of `t_user`
-- ----------------------------
BEGIN;
INSERT INTO `t_user` VALUES ('1303707712332369920', 'admin', '123456', '13222222222', 'admin@qq.com', '超级管理', '1', '21', 'http://ok-admin.xlbweb.cn/images/avatar.png', '0', '2018-11-28 10:00:11', '2020-09-09 23:11:57'), ('1303707712332369921', 'zhangsan', '123456', '13333333333', 'zhangsan@qq.com', '我是张三', '1', '18', 'https://thirdqq.qlogo.cn/qqapp/101235792/F2407217C117BC8E13904E221D393FDC/100', '0', '2018-11-13 15:17:22', '2020-09-09 23:12:00'), ('1303707712336564224', 'lisi', '123456', '13222222222', 'lisi@qq.com', '我是李四', '1', '22', 'https://thirdqq.qlogo.cn/qqapp/101235792/F2407217C117BC8E13904E221D393FDC/100', '0', '2018-11-28 10:00:13', '2020-09-09 23:12:01'), ('1303707712336564225', 'wangwu', '123456', '13222222222', 'wangwu@qq.com', '我是王五', '0', '23', 'https://thirdqq.qlogo.cn/qqapp/101235792/F2407217C117BC8E13904E221D393FDC/100', '0', '2018-11-28 10:00:15', '2020-09-09 23:12:03'), ('1303707712336564226', 'zhaoliu', '123456', '13222222222', 'zhaoliu@qq.com', '我是周六', '1', '19', 'https://thirdqq.qlogo.cn/qqapp/101235792/F2407217C117BC8E13904E221D393FDC/100', '1', '2018-11-28 10:00:17', '2020-09-09 23:12:05'), ('1303707712336564227', 'songqi', '123456', '13222222222', 'songqi@qq.com', '我是宋七', '0', '28', 'https://thirdqq.qlogo.cn/qqapp/101235792/F2407217C117BC8E13904E221D393FDC/100', '1', '2018-11-28 10:00:19', '2020-09-09 23:12:06'), ('1303707712336564228', 'ceshi1', '123456', '13222222222', '43@cc.cc', '我是测试1', '0', '25', 'https://q.qlogo.cn/qqapp/101235792/73EE253B488FB6033AD6A164362F26E6/100', '2', '2018-11-28 10:00:20', '2020-09-09 22:54:27');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
