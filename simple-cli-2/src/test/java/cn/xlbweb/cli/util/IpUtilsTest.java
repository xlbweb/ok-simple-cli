package cn.xlbweb.cli.util;

import cn.xlbweb.util.IpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

@Slf4j
public class IpUtilsTest {

    private static final Log logger = LogFactory.getLog(IpUtilsTest.class);

    @Test
    public void test(){
        log.info("你好");
        logger.info("我爱你");
        String localIp = IpUtils.getLocalIp();
        System.out.println(localIp);
    }
}
