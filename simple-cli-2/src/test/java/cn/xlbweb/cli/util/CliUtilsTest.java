package cn.xlbweb.cli.util;

import cn.xlbweb.cli.AppTests;
import cn.xlbweb.util.IpUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.util.DigestUtils;

import java.util.Date;

public class CliUtilsTest extends AppTests {

    @Autowired
    private JavaMailSender javaMailSender;

    @Test
    public void testGetLocalIp() {
        String localIp = IpUtils.getLocalIp();
        System.out.println(localIp);
    }

    @Test
    public void testSendMail() {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setSubject("密码重置成功");
        simpleMailMessage.setFrom("ok-simple-cli<1451965355@qq.com>");
        simpleMailMessage.setTo("1451965355@qq.com", "2811863207@qq.com");
        simpleMailMessage.setSentDate(new Date());
        simpleMailMessage.setText("恭喜您，密码重置成功，新密码为：123456，为保证您的账号安全，请及时登录系统修改密码！");
        javaMailSender.send(simpleMailMessage);
    }

    @Test
    public void testMd5() {
        String password = "123456";
        String md5Password = DigestUtils.md5DigestAsHex(password.getBytes());
        System.out.println(md5Password);
    }
}