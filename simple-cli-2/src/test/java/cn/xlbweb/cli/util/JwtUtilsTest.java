package cn.xlbweb.cli.util;

import cn.xlbweb.cli.AppTests;
import cn.xlbweb.ji.JwtUtils;
import org.junit.Test;

/**
 * @author: bobi
 * @date: 2021/2/6 下午2:56
 * @description:
 */
public class JwtUtilsTest extends AppTests {

    @Test
    public void test() {
        // 加密
        String token = JwtUtils.encrypt("zhangsan-ADMIN");
        System.out.println(token);
        // 解密
        String parseResult = JwtUtils.decrypt(token);
        System.out.println(parseResult);
        // 账号
        String username = JwtUtils.getUsername(parseResult);
        System.out.println(username);
        // 角色
        String roleName = JwtUtils.getRoleName(parseResult);
        System.out.println(roleName);
    }
}
