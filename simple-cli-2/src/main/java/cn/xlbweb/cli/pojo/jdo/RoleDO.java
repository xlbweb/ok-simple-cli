package cn.xlbweb.cli.pojo.jdo;

import lombok.Data;

import javax.persistence.*;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Entity
@Table(name = "t_role")
@Data
public class RoleDO extends BaseDO {

    private String name;

    private String description;

    private Integer status;
}