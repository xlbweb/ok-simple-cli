package cn.xlbweb.cli.pojo.jdo;

import lombok.Data;

import javax.persistence.*;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Entity
@Table(name = "t_user")
@Data
public class UserDO extends BaseDO {

    private String username;

    private String password;

    private String phone;

    private String mail;

    private Integer status;

    private Integer roleId;
}