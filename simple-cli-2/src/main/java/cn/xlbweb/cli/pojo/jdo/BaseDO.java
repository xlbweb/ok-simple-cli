package cn.xlbweb.cli.pojo.jdo;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * @author: bobi
 * @date: 2021/2/18 下午5:41
 * @description:
 */
@MappedSuperclass
@Data
public class BaseDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Date createTime;

    private Date updateTime;
}
