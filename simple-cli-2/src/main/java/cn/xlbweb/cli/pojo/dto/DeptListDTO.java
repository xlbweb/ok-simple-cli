package cn.xlbweb.cli.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class DeptListDTO {

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty("部门描述")
    private String deptDesc;

    @ApiModelProperty("当前页码，默认1")
    private Integer page = 1;

    @ApiModelProperty("每页显示数量，默认10")
    private Integer size = 10;
}
