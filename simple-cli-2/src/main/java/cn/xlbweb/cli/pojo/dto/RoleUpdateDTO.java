package cn.xlbweb.cli.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2020/9/7 上午12:01
 * @description:
 */
@Data
public class RoleUpdateDTO {

    @ApiModelProperty("主键ID")
    @NotNull(message = "主键ID不能为空")
    private Integer id;

    @ApiModelProperty("角色名称")
    @NotNull(message = "角色名称不能为空")
    private String name;

    @ApiModelProperty("角色描述")
    @NotNull(message = "角色描述不能为空")
    private String description;

    @ApiModelProperty("角色状态 0:正常1:停用")
    @NotNull(message = "角色状态不能为空")
    private String status;
}
