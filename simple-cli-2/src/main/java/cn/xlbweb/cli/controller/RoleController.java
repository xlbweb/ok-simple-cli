package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.pojo.dto.RoleInsertDTO;
import cn.xlbweb.cli.pojo.dto.RoleListDTO;
import cn.xlbweb.cli.pojo.dto.RoleUpdateDTO;
import cn.xlbweb.cli.service.RoleService;
import cn.xlbweb.util.res.ResponseServer;
import cn.xlbweb.util.res.ResponseTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author: bobi
 * @date: 2020/9/6 下午6:36
 * @description:
 */
@RestController
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/roles/{id}")
    public ResponseServer getRole(@PathVariable Integer id) {
        return roleService.getRole(id);
    }

    @GetMapping("/roles")
    public ResponseTable listRole(@Valid RoleListDTO dto) {
        return roleService.listRole(dto);
    }

    @PostMapping("/role")
    public ResponseServer insertRole(@Valid @RequestBody RoleInsertDTO dto) {
        return roleService.insertRole(dto);
    }

    @PutMapping("/role")
    public ResponseServer updateRole(@Valid @RequestBody RoleUpdateDTO dto) {
        return roleService.updateRole(dto);
    }

    @DeleteMapping("/role/{ids}")
    public ResponseServer deleteRole(@PathVariable String ids) {
        return roleService.deleteRole(ids);
    }
}
