package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.pojo.dto.DeptListDTO;
import cn.xlbweb.cli.service.DeptService;
import cn.xlbweb.util.res.ResponseServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: bobi
 * @date: 2021/2/18 下午5:32
 * @description:
 */
@RestController
public class DeptController {

    @Autowired
    private DeptService deptService;

    @GetMapping("/dept/{id}")
    public ResponseServer getDept(@PathVariable Integer id) {
        return deptService.getDept(id);
    }

    @GetMapping("/depts")
    public ResponseServer listDept(DeptListDTO dto) {
        return deptService.listDept(dto);
    }
}
