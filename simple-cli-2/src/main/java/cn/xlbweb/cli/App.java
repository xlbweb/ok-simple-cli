package cn.xlbweb.cli;

import cn.xlbweb.cli.common.CommonProperties;
import cn.xlbweb.util.IpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
@Slf4j
@SpringBootApplication
public class App implements ApplicationRunner {

    @Value("${server.port}")
    public int port;

    @Autowired
    private CommonProperties commonProperties;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Tomcat: http://{}:{}", IpUtils.getLocalIp(), port);
        log.info("Api: http://{}:{}{}", IpUtils.getLocalIp(), port, commonProperties.getSwaggerUri());
    }

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
