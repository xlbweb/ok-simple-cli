package cn.xlbweb.cli.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;

import java.util.Date;

/**
 * @author: bobi
 * @date: 2018/11/18 20:53
 * @description: 系统工具类
 */
@Slf4j
public class CliUtils {

    /**
     * 邮件发送者
     */
    private static String MAIL_FROM = "ok-simple-cli<1451965355@qq.com>";

    /**
     * 重置密码邮件模板
     *
     * @param to
     * @param newPwd
     * @return
     */
    public static SimpleMailMessage buildResetPwd(String to, String newPwd) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setSubject("密码重置成功");
        simpleMailMessage.setFrom(MAIL_FROM);
        simpleMailMessage.setTo(to);
        simpleMailMessage.setSentDate(new Date());
        simpleMailMessage.setText("恭喜您，密码重置成功，新密码为：" + newPwd + "，为保证您的账号安全，请及时登录系统修改密码！");
        return simpleMailMessage;
    }
}
