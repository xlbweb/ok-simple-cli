package cn.xlbweb.cli.service.impl;

import cn.xlbweb.cli.common.CommonConstant;
import cn.xlbweb.cli.dao.RoleRepository;
import cn.xlbweb.cli.pojo.dto.RoleInsertDTO;
import cn.xlbweb.cli.pojo.dto.RoleListDTO;
import cn.xlbweb.cli.pojo.dto.RoleUpdateDTO;
import cn.xlbweb.cli.pojo.jdo.RoleDO;
import cn.xlbweb.cli.pojo.vo.RoleVO;
import cn.xlbweb.cli.service.RoleService;
import cn.xlbweb.util.ConvertUtils;
import cn.xlbweb.util.res.ResponseServer;
import cn.xlbweb.util.res.ResponseTable;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public ResponseServer getRole(Integer id) {
        Map<String, Object> result = roleRepository.getRoleById(id);
        if (CollectionUtils.isEmpty(result)) {
            return ResponseServer.error("角色不存在");
        }
        return ResponseServer.success("查询角色成功", result);
    }

    @Override
    public ResponseTable<List<RoleVO>> listRole(RoleListDTO dto) {
        Specification<RoleDO> specification = (Specification<RoleDO>) (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (StringUtils.isNotBlank(dto.getName())) {
                predicates.add(criteriaBuilder.like(root.get("name"), "%" + dto.getName() + "%"));
            }
            if (StringUtils.isNotBlank(dto.getDescription())) {
                predicates.add(criteriaBuilder.like(root.get("description"), "%" + dto.getDescription() + "%"));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
        PageRequest pageRequest = PageRequest.of(dto.getPage() - 1, dto.getSize(), Sort.Direction.DESC, CommonConstant.DEFAULT_ORDER_COLUMN);
        Page<RoleDO> page = roleRepository.findAll(specification, pageRequest);
        List<RoleVO> roleVOs = ConvertUtils.copyListProperties(page.getContent(), RoleVO::new);
        return ResponseTable.success("查询成功", page.getTotalElements(), roleVOs);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseServer insertRole(RoleInsertDTO dto) {
        RoleDO checkRoleDO = roleRepository.findByName(dto.getName());
        if (Objects.nonNull(checkRoleDO)) {
            return ResponseServer.error("角色已存在");
        }
        int count = roleRepository.insertRole(dto);
        if (count > 0) {
            return ResponseServer.success("添加角色成功");
        }
        return ResponseServer.error("添加角色   失败");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseServer updateRole(RoleUpdateDTO dto) {
        int count = roleRepository.updateRole(dto);
        if (count > 0) {
            return ResponseServer.success("更新角色成功");
        }
        return ResponseServer.error("更新角色失败");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseServer deleteRole(String ids) {
        int count = roleRepository.deleteRole(ConvertUtils.strToIntegerList(ids));
        if (count > 0) {
            return ResponseServer.success("删除角色成功");
        }
        return ResponseServer.error("删除角色失败");
    }
}
