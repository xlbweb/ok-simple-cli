package cn.xlbweb.cli.service;

import cn.xlbweb.cli.pojo.dto.DeptListDTO;
import cn.xlbweb.util.res.ResponseServer;

/**
 * @author: bobi
 * @date: 2021/2/18 下午5:32
 * @description:
 */
public interface DeptService {

    /**
     * 根据id查询部门
     *
     * @param id
     * @return
     */
    ResponseServer getDept(Integer id);

    /**
     * 模糊搜索部门
     *
     * @param dto
     * @return
     */
    ResponseServer listDept(DeptListDTO dto);
}
