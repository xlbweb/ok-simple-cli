package cn.xlbweb.cli.service;

import cn.xlbweb.cli.pojo.dto.RoleInsertDTO;
import cn.xlbweb.cli.pojo.dto.RoleListDTO;
import cn.xlbweb.cli.pojo.dto.RoleUpdateDTO;
import cn.xlbweb.cli.pojo.vo.RoleVO;
import cn.xlbweb.util.res.ResponseServer;
import cn.xlbweb.util.res.ResponseTable;

import java.util.List;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
public interface RoleService {

    /**
     * 根据id查询角色
     *
     * @param id
     * @return
     */
    ResponseServer<RoleVO> getRole(Integer id);

    /**
     * 模糊查询角色
     *
     * @param dto
     * @return
     */
    ResponseTable<List<RoleVO>> listRole(RoleListDTO dto);

    /**
     * 添加角色
     *
     * @param dto
     * @return
     */
    ResponseServer insertRole(RoleInsertDTO dto);

    /**
     * 更新角色
     *
     * @param dto
     * @return
     */
    ResponseServer updateRole(RoleUpdateDTO dto);

    /**
     * 删除角色
     *
     * @param ids
     * @return
     */
    ResponseServer deleteRole(String ids);
}
