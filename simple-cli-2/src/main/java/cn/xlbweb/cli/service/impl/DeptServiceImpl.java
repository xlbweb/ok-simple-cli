package cn.xlbweb.cli.service.impl;

import cn.xlbweb.cli.pojo.dto.DeptListDTO;
import cn.xlbweb.cli.service.DeptService;
import cn.xlbweb.cli.util.JdbcUtils;
import cn.xlbweb.util.res.ResponseServer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author: bobi
 * @date: 2021/2/18 下午5:32
 * @description:
 */
@Service
public class DeptServiceImpl implements DeptService {

    @Override
    public ResponseServer getDept(Integer id) {
        String sql = "select * from t_dept where id = ?";
        Object[] params = {id};
        Map<String, Object> result = JdbcUtils.getOne(sql, params);
        if (!CollectionUtils.isEmpty(result)) {
            return ResponseServer.success("查询部门成功", result);
        }
        return ResponseServer.error("部门不存在");
    }

    @Override
    public ResponseServer listDept(DeptListDTO dto) {
        StringBuilder sql = new StringBuilder();
        sql.append("select * from t_dept where 1=1");
        List<Object> params = new ArrayList<>();
        if (StringUtils.isNotBlank(dto.getDeptName())) {
            sql.append(" and dept_name like ?");
            params.add("%" + dto.getDeptName() + "%");
        }
        if (StringUtils.isNotBlank(dto.getDeptDesc())) {
            sql.append(" and dept_desc like ?");
            params.add("%" + dto.getDeptDesc() + "%");
        }
        List<Map<String, Object>> results = JdbcUtils.findAll(sql.toString(), params.toArray(), dto.getPage() - 1, dto.getSize());
        return ResponseServer.success("查询部门成功", results);
    }
}
