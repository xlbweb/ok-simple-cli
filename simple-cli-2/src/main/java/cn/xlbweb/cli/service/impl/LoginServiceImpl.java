package cn.xlbweb.cli.service.impl;

import cn.xlbweb.cli.dao.RoleRepository;
import cn.xlbweb.cli.dao.UserRepository;
import cn.xlbweb.cli.pojo.jdo.RoleDO;
import cn.xlbweb.cli.pojo.jdo.UserDO;
import cn.xlbweb.cli.pojo.dto.LoginDTO;
import cn.xlbweb.cli.service.LoginService;
import cn.xlbweb.ji.JwtUtils;
import cn.xlbweb.util.res.ResponseServer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Objects;

/**
 * @author: bobi
 * @date: 2021/1/31 下午10:17
 * @description:
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public ResponseServer<String> login(LoginDTO dto) {
        String md5Password = DigestUtils.md5DigestAsHex(dto.getPassword().getBytes());
        UserDO userDO = userRepository.findByUsernameAndPassword(dto.getUsername(), md5Password);
        if (Objects.nonNull(userDO)) {
            // 约定: 账号-角色名 (jack-manager)
            RoleDO roleDO = roleRepository.getOne(userDO.getRoleId());
            String userInfo = StringUtils.join(userDO.getUsername(), "-", roleDO.getName());
            return ResponseServer.success("登录成功", JwtUtils.encrypt(userInfo));
        }
        return ResponseServer.error("登录失败，账号或密码错误");
    }
}
