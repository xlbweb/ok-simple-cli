package cn.xlbweb.cli.dao;

import cn.xlbweb.cli.pojo.dto.RoleInsertDTO;
import cn.xlbweb.cli.pojo.dto.RoleUpdateDTO;
import cn.xlbweb.cli.pojo.jdo.RoleDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

/**
 * @author: bobi
 * @date: 2020/11/30 下午12:16
 * @description:
 */
public interface RoleRepository extends JpaRepository<RoleDO, Integer>, JpaSpecificationExecutor<RoleDO> {

    /**
     * 根据id查询角色
     *
     * @param id
     * @return
     */
    @Query(value = "select * from t_user where id = ?1", nativeQuery = true)
    Map<String, Object> getRoleById(Integer id);

    /**
     * 根据角色名称查询
     *
     * @param name
     * @return
     */
    RoleDO findByName(String name);

    /**
     * 添加角色
     * 默认值：
     * 1) status状态 -> 1 禁用
     * 2) create_time创建时间 -> now()
     *
     * @param dto
     * @return
     */
    @Modifying
    @Query(value = "insert t_role(name, description, status, create_time) values(:#{#dto.name}, :#{#dto.description}, 1, now())", nativeQuery = true)
    int insertRole(RoleInsertDTO dto);

    /**
     * 修改角色
     * 默认值：
     * 1) update_time创建时间 -> now()
     *
     * @param dto
     * @return
     */
    @Modifying
    @Query(value = "update t_role set name = :#{#dto.name}, description = :#{#dto.description}, status = :#{#dto.status}, update_time = now() where id = :#{#dto.id}", nativeQuery = true)
    int updateRole(RoleUpdateDTO dto);

    /**
     * 删除角色
     *
     * @param ids
     * @return
     */
    @Modifying
    @Query(value = "delete from t_role where id in (?1)", nativeQuery = true)
    int deleteRole(List<Integer> ids);
}
