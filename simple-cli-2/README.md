> 涉及技术

<img src="https://img.shields.io/badge/jdk-1.8-brightgreen">
<img src="https://img.shields.io/badge/mysql-8.0.13-brightgreen">
<img src="https://img.shields.io/badge/maven-3.6.1-brightgreen">
<br/>
<img src="https://img.shields.io/badge/springboot-2.2.7.RELEASE-brightgreen">
<img src="https://img.shields.io/badge/spring--data--jpa-2.2.7.RELEASE-brightgreen">
<img src="https://img.shields.io/badge/spring--data--jdbc-2.2.7.RELEASE-brightgreen">
<br/>
<img src="https://img.shields.io/badge/swagger-3.0.0-brightgreen">
<img src="https://img.shields.io/badge/ok--util-1.0.0-brightgreen">
<img src="https://img.shields.io/badge/ok--jwt--interceptor-1.0.0-brightgreen">
<br/>

> 详细功能点

- 数据库/数据库表手动创建，create_time 和 update_time 字段不设置 CURRENT_TIMESTAMP，使用 `setCreateTime(new Date())` 或 `now()` 函数来解决；

> 总结

只涉及单表查询操作的，就用 XxxRepository 来解决；

涉及多表查询操作的，就使用 JPA 的原生 sql 语句来解决；

因为数据库建表语句和 JPA 实体类并没有设置外键关联关系，故在做添加/更新/删除操作时，从表关联记录应在业务层代码编写执行。